var stompSeries = null;

function connect() {
    var socket = new SockJS('/websocket');
    stompSeries = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompSeries.subscribe('/stomp/series', function (res) {
            var res = JSON.parse(res.body);
            handleChatResponse(res.message, res.user);
        });
    });
}

function disconnect() {
    if (stompSeries !== null) {
        stompSeries.disconnect();
    }
    console.log("Disconnected");
}

function sendChat() {
    var chat = {

        'message': $("#series-message").val(),
        'user': $("#username").html()
    }
    stompSeries.send("/series", {}, JSON.stringify(chat));
}

function handleChatResponse(message, user) {
    $("#series-chat ul").append("<li class=\"msg-left\">"+user+": "+message+"</li>");
}


// $(function () {
//     $("form").on('submit', function (e) {
//         e.preventDefault();
//     });
//     connect();
//     $( "#send" ).click(function() { sendChat(); });
// });


$(document).ready(function(){

    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    connect();
    $( "#series-send" ).click(function() { sendChat(); });

    // $('.msg_head').click(function(){
    //     var chatbox = $(this).parents().attr("rel") ;
    //     $('[rel="'+chatbox+'"] .msg_wrap').slideToggle('slow');
    //     return false;
    // });
    //
    // $('.close').click(function(){
    //
    //     var chatbox = $(this).parents().parents().attr("rel") ;
    //     $('[rel="'+chatbox+'"]').hide();
    //     //update require
    //     arr.splice($.inArray(chatbox, arr),1);
    //     i = 50 ; // start position
    //     j = 260;  //next position
    //     $.each( arr, function( index, value ) {
    //         $('[rel="'+value+'"]').css("right",i);
    //         i = i+j;
    //     });
    //
    //     return false;
    // });

});