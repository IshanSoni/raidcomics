function buildPage(jsonString) {
    // jsonString = jsonString.replace(/'/g, '"');
    // console.log(jsonString);

    var canvas = new fabric.Canvas('view-canvas');
    // canvas.loadFromJSON(jsonString);
    canvas.setHeight(500);
    canvas.setWidth(575);
    canvas.loadFromJSON(jsonString, canvas.renderAll.bind(canvas), function(o, object) {
        object.set('selectable', false);
    });

    var page = parseInt($("#page").val());
    if(page === 1 || page === 0) {
        console.log("aaaaaa");
        document.getElementById("prev").style.visibility = "hidden";
    }
}

function prevPage() {
    var series = document.getElementById("series").value;
    var book = document.getElementById("book").value;
    var page = document.getElementById("page").value;

    window.location.href = "/view?series="+series+"&book="+book+"&page="+(parseInt(page)-1);
}

function nextPage() {
    var series = document.getElementById("series").value;
    var book = document.getElementById("book").value;
    var page = document.getElementById("page").value;

    window.location.href = "/view?series="+series+"&book="+book+"&page="+(parseInt(page)+1);
}

// $(function () {
//     // computerWordID = $("#computer-word-id").val();
//     $( "#prev" ).click(function() { prevPage(); });
//     $( "#next" ).click(function() { nextPage(); });
// });