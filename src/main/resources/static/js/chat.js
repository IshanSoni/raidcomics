var stompClient = null;

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/stomp/chat', function (res) {
            var res = JSON.parse(res.body);
            handleChatResponse(res.message, res.user, res.series);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function sendGlobalChat() {
    var chat = {
        'message': $("#message").val(),
        'user': $("#username").html(),
        'series': 'global'
    }
    stompClient.send("/chat", {}, JSON.stringify(chat));
    $("#message").val("");
}

function sendSeriesChat() {
    var chat = {
        'message': $("#series-message").val(),
        'user': $("#username").html(),
        'series': $("#series").val()
    }
    stompClient.send("/chat", {}, JSON.stringify(chat));
    $("#series-message").val("");
}

function handleChatResponse(message, user, series) {
    if(series === "global") {
        $("#chat ul").append("<li class=\"msg-left\">"+user+": "+message+"</li>");
    } else {
        if(series === $("#series").val()) {
            $("#series-chat ul").append("<li class=\"msg-left\">"+user+": "+message+"</li>");
        }
    }
}

$(document).ready(function(){

    $("#chatform").on('submit', function (e) {
        e.preventDefault();
    });
    connect();
    $( "#send" ).click(function() { sendGlobalChat(); });
    $( "#series-send" ).click(function() { sendSeriesChat(); });

    $('.msg_head').click(function(){
        var chatbox = $(this).parents().attr("rel") ;
        $('[rel="'+chatbox+'"] .msg_wrap').slideToggle('slow');
        return false;
    });
});