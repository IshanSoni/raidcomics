package raidcomics.models;

import raidcomics.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findByAcctID(String acctID);
    User findByUsername(String username);
}
