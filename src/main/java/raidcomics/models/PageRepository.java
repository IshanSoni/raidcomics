                package raidcomics.models;

import raidcomics.models.Page;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


public interface PageRepository extends MongoRepository<Page, String>{
    Page findByPageID(String pageID);
 /*
    @Query("{ 'bookID' : { $regex: ?0 } }")
    List<Page> findByRegexpbookID(String regexp);
  */
    
}
