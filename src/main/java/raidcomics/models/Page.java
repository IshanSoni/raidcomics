package raidcomics.models;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "pages")

public class Page{
    @Id
    private String pageID;
    private String bookID;
    private String seriesID;
    private String acctID;
    private String pageNum;
    private String pageData;

    @CreatedDate
    private Date creationDate;

    public Page() {

    }

    public String getPageID(){
        return this.pageID;
    }

    public String getPageNum() { return this.pageNum; }

    public String getBookID(){
        return this.bookID;
    }

    public String getSeriesID(){
        return this.seriesID;
    }

    public String getPageData(){
        return this.pageData;
    }

    public String getAcctID(){
        return this.acctID;
    }

    public Date getCreationDate(){
        return this.creationDate;
    }

    public void setPageID(String newID){
        this.pageID = newID;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public void setBookID(String newID){
        this.bookID = newID;
    }

    public void setSeriesID(String newID){
        this.seriesID = newID;
    }

    public void setPageData(String newData){
        this.pageData = newData;
    }

    public void setAcctID(String newID){
        this.acctID = newID;
    }

    public void setCreationDate(Date newDate){
        this.creationDate = newDate;
    }
}