package raidcomics.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "books")

public class ComicBook{
    @Id
    private String bookID;
    private String seriesID;
    private String bookName;
    private String acctID;
    private String bookCoverArt;
    private int likes;
    private int dislikes;
    private int numPages;
    private boolean downloadable;
    private boolean isPublic;
    private boolean fansOnly;


    @CreatedDate
    private Date creationDate;

    private ArrayList<String> pages;

    public boolean isPublic() {return isPublic;}

    public void setPublic(boolean ans){this.isPublic = ans;}

    public ComicBook() {
        pages = new ArrayList<>();
    }

    public String getBookID(){
        return this.bookID;
    }

    public void setBookID(String newID){
        this.bookID = newID;
    }

    public String getSeriesID(){
        return this.seriesID;
    }

    public void setSeriesID(String newID){
        this.seriesID = newID;
    }

    public boolean isFansOnly() {return fansOnly;}

    public void setFansOnly(boolean ans){this.fansOnly = ans;}

    public String getBookName(){
        return this.bookName;
    }

    public void setBookName(String newName){
        this.bookName = newName;
    }

    public String getAcctID(){
        return this.acctID;
    }

    public void setAcctID(String newID){
        this.acctID = newID;
    }

    public int getLikes(){
        return this.likes;
    }

    public void setLikes(int newVal){
        this.likes = newVal;
    }

    public int getDislikes(){
        return this.dislikes;
    }

    public void setDislikes(int newVal){
        this.dislikes = newVal;
    }

    public int getNumPages(){
        return this.numPages;
    }

    public void setNumPages(int newVal){
        this.numPages = newVal;
    }

    public ArrayList<String> getPages() {
        return this.pages;
    }

    public void setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isDownloadable() {
        return this.downloadable;
    }

    public void setPages(ArrayList<String> pages) {
        this.pages = pages;
    }

    public String getBookCoverArt(){return this.bookCoverArt;}
    public void setBookCoverArt(String artData){
        this.bookCoverArt = artData;
    }
//    public void addPage(Page a){
//        this.pages.add(a);
//        this.numPages = this.numPages + 1;
//    }
}
