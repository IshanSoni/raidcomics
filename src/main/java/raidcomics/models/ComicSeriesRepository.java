package raidcomics.models;
import org.springframework.data.mongodb.repository.Query;
import raidcomics.models.ComicSeries;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface ComicSeriesRepository  extends MongoRepository<ComicSeries, String>{
    ComicSeries findBySeriesID(String seriesID);
    List<ComicSeries> findByCategory(String category);
    //ComicSeries findBySeriesName(String seriesName);

    @Query("{ 'seriesName' : { $regex: ?0 } }")
    List<ComicSeries> findByRegexpSeriesName(String regexp);
    @Query("{ 'category' : { $regex: ?0 } }")
    List<ComicSeries> findByRegexpCategory(String regexp);
    @Query("{ 'acctID' : { $regex: ?0 } }")
    List<ComicSeries> findByRegexpUser(String regexp);
}
