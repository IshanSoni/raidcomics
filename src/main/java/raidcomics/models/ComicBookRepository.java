package raidcomics.models;
import raidcomics.models.ComicBook;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface ComicBookRepository extends MongoRepository<ComicBook, String>{
    ComicBook findByBookID(String bookID);
}
