package raidcomics.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "series")
public class ComicSeries implements Comparable<ComicSeries>{
    @Id
    private String seriesID;
    private String acctID;//this is the author
    private String seriesCoverArt;

    private String seriesName;
    private String category;
    private int numBooks;
    private int likes;
    private int dislikes;
    private int numRatings;
    private boolean isPublic;
    private boolean fansOnly;

    @CreatedDate
    private Date creationDate;


    private int numFans;
    private ArrayList<String> books;

    public ComicSeries() {
        books = new ArrayList<>();
    }

    public String getAcctID(){
        return this.acctID;
    }

    public boolean isPublic() { return isPublic;}

    public boolean isFansOnly() {return fansOnly;}

    public void setFansOnly(boolean ans){this.fansOnly = ans;}

    public void setPublic(boolean ans){this.isPublic = ans;}

    public void setAcctID(String newID){
        this.acctID = newID;
    }

    public void setSeriesID(String newID){
        this.seriesID = newID;
    }

    public String getSeriesID(){
        return this.seriesID;
    }

    public String getSeriesName(){
        return this.seriesName;
    }

    public void setSeriesName(String newName){
        this.seriesName = newName;
    }

    public String getCategory(){
        return this.category;
    }

    public void setCategory(String newCat){
        this.category = newCat;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date newDate){
        this.creationDate = newDate;
    }

    public int getLikes(){return this.likes;}
    public int getDislikes(){return this.dislikes;}

    public void setLikes(int newLikes){this.likes = newLikes;}
    public void setDislikes(int newDis){this.dislikes = newDis;}
    public int getNumFans() {
        return this.numFans;
    }

    public void setNumFans(int numFans) {
        this.numFans = numFans;
    }

    public int getNumBooks() {
        return this.numBooks;
    }

    public void setNumBooks(int numBooks) {
        this.numBooks = numBooks;
    }

    public void setBooks(ArrayList<String> books) {
        this.books = books;
    }

    public int getNumRatings(){
        return this.numRatings;
    }
    public void setNumRatings(int numRatings){
        this.numRatings = numRatings;
    }

    public String getCoverArt(){return this.seriesCoverArt;}
    public void setCoverArt(String coverArtData){
        this.seriesCoverArt = coverArtData;
    }
    public ArrayList<String> getBooks(){
        return this.books;
    }

    public int compareTo(ComicSeries series){
        return this.seriesName.toLowerCase().compareTo(series.seriesName.toLowerCase());
    }

}