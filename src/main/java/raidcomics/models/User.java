package raidcomics.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Document(collection = "users")
public class User implements UserDetails {
    @Id
    private String acctID;

    @Indexed(unique = true, direction = IndexDirection.DESCENDING)
    private String username;
    private String email;
    @Indexed(sparse = true, direction = IndexDirection.DESCENDING)
    private String password;
    private String firstname;
    private String lastname;
    private ArrayList<String> favouriteSeries;
    private ArrayList<String> comicUploads;
    private ArrayList<String> pageDrafts;
    private ArrayList<String> bookDrafts;
    private ArrayList<String> followers;
    private ArrayList<String> friends;
    private ArrayList<Integer> readingStats;

    @CreatedDate
    private Date joinedDate;


    public User() {
        favouriteSeries = new ArrayList<>();
        comicUploads = new ArrayList<>();
        pageDrafts = new ArrayList<>();
        bookDrafts = new ArrayList<>();
        followers = new ArrayList<>();
        friends = new ArrayList<>();
        readingStats = new ArrayList<>();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){
        Collection authorities = new ArrayList();
        authorities.add("user");
        return authorities;
    }


    public String getAcctID() {
        return acctID;
    }

    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public String getUsername() {
        return username;
    }

    //These methods may need to be modified, these are just the defaults
    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public void setUsername(String email) {
        this.username = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public ArrayList<String> getFavouriteSeries() {
        return favouriteSeries;
    }

    public ArrayList<String> getComicUploads() {
        return comicUploads;
    }

    public void setComicUploads(ArrayList<String> comicUploads) {
        this.comicUploads = comicUploads;
    }

    public ArrayList<String> getBookDrafts() {
        return bookDrafts;
    }

    public ArrayList<Integer> getReadingStats() {
        return readingStats;
    }

    public ArrayList<String> getPageDrafts() {
        return pageDrafts;
    }

    public void setFavouriteSeries(ArrayList<String> favouriteSeries) {
        this.favouriteSeries = favouriteSeries;
    }

    public ArrayList<String> getFollowers() {
        return followers;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void setBookDrafts(ArrayList<String> bookDrafts) {
        this.bookDrafts = bookDrafts;
    }

    public Date getJoinedDate() {
        return joinedDate;
    }

    public void setFollowers(ArrayList<String> followers) {
        this.followers = followers;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
    }

    public void setJoinedDate(Date joinedDate) {
        this.joinedDate = joinedDate;
    }

    public void setPageDrafts(ArrayList<String> pageDrafts) {
        this.pageDrafts = pageDrafts;
    }

    public void setReadingStats(ArrayList<Integer> readingStats) {
        this.readingStats = readingStats;
    }
}
