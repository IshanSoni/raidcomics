package raidcomics.models;

public class ChatSent {
    private String message;
    private String user;
    private String series;

    public ChatSent() {}

    public ChatSent(String message, String user, String series) {
        this.message = message;
        this.user = user;
        this.series = series;
    }

    public String getMessage() {
        return message;
    }

    public String getUser() {
        return user;
    }

    public String getSeries() {
        return series;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
