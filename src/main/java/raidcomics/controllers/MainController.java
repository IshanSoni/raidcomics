package raidcomics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import raidcomics.models.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@RestController
public class MainController {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ComicSeriesRepository seriesRepo;
    @Autowired
    private ComicBookRepository booksRepo;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView homeDisplay() {
        ModelAndView mv = new ModelAndView("index");

        List<ComicSeries> actionComics = seriesRepo.findByRegexpCategory("Action");
        List<ComicSeries> adventureComics = seriesRepo.findByRegexpCategory("Adventure");
        List<ComicSeries> mysteryComics = seriesRepo.findByRegexpCategory("Mystery");
        List<ComicSeries> comedyComics = seriesRepo.findByRegexpCategory("Comedy");
        List<ComicSeries> fantasyComics = seriesRepo.findByRegexpCategory("Fantasy");
        List<ComicSeries> horrorComics = seriesRepo.findByRegexpCategory("Horror");
        List<ComicSeries> romanceComics = seriesRepo.findByRegexpCategory("Romance");
        List<ComicSeries> allComics = seriesRepo.findAll();
        Collections.sort(allComics);
        System.out.println(allComics);
        mv.addObject("allcomics",allComics);
        mv.addObject("action", actionComics);
        mv.addObject("adventure", adventureComics);
        mv.addObject("mystery", mysteryComics);
        mv.addObject("comedy", comedyComics);
        mv.addObject("fantasy", fantasyComics);
        mv.addObject("horror", horrorComics);
        mv.addObject("romance", romanceComics);

        return mv;

    }
    // Profile page, can move this later if needed
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView viewProfile() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("profile");
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);
        String userID = user.getAcctID();
        System.out.println(userID);
        List<ComicSeries> userComics = seriesRepo.findByRegexpUser(userID);
        System.out.println(userComics);
        mv.addObject("usercomics",userComics);
        mv.addObject("user", user);
        return mv;
    }
    //search bar
    @RequestMapping(value="/search", method = RequestMethod.GET)
    public ModelAndView Search(@RequestParam(value="q", required = false)String searchTerm){
        StringBuilder sb = new StringBuilder();
        for(char c : searchTerm.toCharArray()) {
            sb.append('[');
            sb.append(Character.toUpperCase(c));
            sb.append(Character.toLowerCase(c));
            sb.append(']');
        }
        String searchTermRegex = sb.toString();

        List<ComicSeries> searchResultByName = seriesRepo.findByRegexpSeriesName(searchTermRegex);
        List<ComicSeries> searchResultByCategory = seriesRepo.findByRegexpCategory(searchTermRegex);

        ModelAndView mv = new ModelAndView("search");
        mv.addObject("searchResultByName", searchResultByName);
        mv.addObject("searchResultByCategory", searchResultByCategory);
        return mv;
    }
}