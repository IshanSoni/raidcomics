package raidcomics.controllers;


import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import raidcomics.models.*;

import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Date;

@RestController
public class AddController {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ComicSeriesRepository seriesRepo;
    @Autowired
    private ComicBookRepository bookRepo;
    @Autowired
    private PageRepository pageRepo;

    @RequestMapping (value = {"/edit"}, method = RequestMethod.POST)
    public ModelAndView handleFileUpload(@RequestParam("file") MultipartFile jsonFile) throws Exception {
        ModelAndView mv = new ModelAndView();
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            //user not logged in, cant save

        }
        
        if(!jsonFile.getContentType().equals("application/json")) {
            //if uploaded file type is not json
        }

        String pageJson = new String(jsonFile.getBytes());
        mv.setViewName("edit");
        mv.addObject("page", pageJson);
        return mv;
    }

    @RequestMapping(value = "/addcomicseries", method = RequestMethod.POST)
    public ModelAndView addComicSeries(@RequestParam("series-name") String seriesName, @RequestParam("category") String category, @RequestParam("series-cover-art") String seriesCoverArt){
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);

        ComicSeries series = new ComicSeries();
        Date creationDate = new Date();
        series.setCreationDate(creationDate);
        series.setSeriesName(seriesName);
        series.setCategory(category);
        series.setAcctID(user.getAcctID());
        series.setCoverArt(seriesCoverArt);
        seriesRepo.save(series);
        user.getComicUploads().add(series.getSeriesID());
        userRepo.save(user);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/editseries?series="+series.getSeriesID());
        return mv;
    }


    @RequestMapping(value = {"/addcomicbook"}, method = RequestMethod.POST)
    public ModelAndView addComicBook(@RequestParam("book") String bookName, @RequestParam("series") String seriesID, @RequestParam("book-cover-art") String bookCoverArt) {
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);

        ComicBook book = new ComicBook();
        Date creationDate = new Date();
        book.setCreationDate(creationDate);
        book.setBookName(bookName);
        book.setAcctID(user.getAcctID());
        book.setSeriesID(seriesID);
        book.setBookCoverArt(bookCoverArt);
        bookRepo.save(book);
        //need to create and set unique BookID
        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
        series.getBooks().add(book.getBookID());
        seriesRepo.save(series);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/editseries?series="+seriesID);
        return mv;
    }
    
    @RequestMapping(value = {"/addcomicbookpage"}, method = RequestMethod.POST)
    public ModelAndView addComicBookPage(@RequestParam("series") String seriesID, @RequestParam("book") String bookID){
        String username = ((UserDetails) 
SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);
        
        Page page = new Page();
        Date creationDate = new Date();
        page.setCreationDate(creationDate);
        page.setSeriesID(seriesID);
        page.setBookID(bookID);
        page.setAcctID(user.getAcctID());
        page.setPageData("");
        
        ComicBook book = bookRepo.findByBookID(bookID);
        page.setPageNum(Integer.toString(book.getPages().size()+1));
        pageRepo.save(page);

        book.getPages().add(page.getPageID());
        bookRepo.save(book);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/editbook?series="+seriesID+"&book="+bookID);
        return mv;
               
    }
    
/*
    @RequestMapping(value = {"/addcomicpage"}), method = RequestMethod.POST)
    public ModelAndView addComicPage(@RequestParm("numPages")) String numPages, @RequestParam("series") String seriesID, @RequestParam("book") String bookID){
        
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);
        
        Page page = new Page();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/editbooks?series="+seriesID);
        return mv;
        
    }*/

//    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
//    public ModelAndView handleFileUpload(@RequestParam("file") MultipartFile jsonMPFile) throws Exception {
//        ModelAndView mv = new ModelAndView();
//        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
//            //user not logged in, cant save
//
//        }
//        if(!jsonMPFile.getContentType().equals("application/json")) {
//            //if uploaded file type is not json
//
//        }
//
//        File jsonFile = convertToFile(jsonMPFile);
//
//        JSONParser parser = new JSONParser();
//        JSONObject json = (JSONObject) parser.parse(new FileReader(jsonFile));
//
//        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
//        User user = userRepo.findByUsername(username);
//        String seriesID = addSeriesToDB(json, user);
//
//        String url = "/view?series="+seriesID+"&book=1&page=1";
//        mv.setViewName("redirect:"+url);
//        return mv;
//    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.GET)
    public ModelAndView showFileUpload() {
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            //user not logged in, cant upload
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("add");
        return mv;
    }

    public File convertToFile(MultipartFile file) throws Exception {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public String addSeriesToDB(JSONObject json, User user) {
        ComicSeries series = new ComicSeries();
        series.setSeriesName(json.get("name").toString());
        series.setCategory(json.get("category").toString());
        seriesRepo.save(series);

        String seriesID = series.getSeriesID();

        JSONArray books = (JSONArray) json.get("books");
        for(Object o : books) {
            JSONObject bookJson = (JSONObject) o;

            ComicBook book = new ComicBook();
            book.setBookName(bookJson.get("name").toString());
            book.setSeriesID(seriesID);
            bookRepo.save(book);

            String bookID = book.getBookID();
            series.getBooks().add(bookID);

            JSONArray pages = (JSONArray) bookJson.get("pages");
            for(Object p : pages) {
                JSONObject pageJson = (JSONObject) p;

                Page page = new Page();
                page.setSeriesID(seriesID);
                page.setBookID(bookID);
                page.setPageData(pageJson.toString());
                pageRepo.save(page);

                String pageID = page.getPageID();
                book.getPages().add(pageID);
            }
            bookRepo.save(book);
        }
        seriesRepo.save(series);

        user.getComicUploads().add(seriesID);
        userRepo.save(user);

        return seriesID;
    }

    //@RequestMapping(value = "/newSeries", method = RequestMethod.GET)
    /*public ModelAndView createNewSeries(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("newSeries");
        return modelAndView;
    }*/

    @RequestMapping(value = "/newseries", method = RequestMethod.GET)
    public ModelAndView newSeries(){
        ModelAndView modelAndView = new ModelAndView();
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            //user not logged in, cant
        }
        modelAndView.setViewName("newseries");
        return modelAndView;
    }

//    @RequestMapping (value = "/comicseries", method = RequestMethod.GET)
//    public ModelAndView showForm(){
//        return new ModelAndView("comicSeriesHome", "comicseries", new ComicSeries());
//    }

    @RequestMapping(value = {"/adddummycomic"}, method = RequestMethod.POST)
    public String dummypost(@RequestParam("file") MultipartFile jsonMPFile) throws Exception {


        File jsonFile = convertToFile(jsonMPFile);

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(new FileReader(jsonFile));

        String username = json.get("username").toString();
        User user = userRepo.findByUsername(username);
        String seriesID = addSeriesToDB(json, user);

        return seriesID;
    }

    @RequestMapping(value = {"/adddummycomic"}, method = RequestMethod.GET)
    public ModelAndView dummyget() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("testdummy");
        return mv;
    }




}