package raidcomics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import raidcomics.models.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EditController {


    @Autowired
    private UserRepository userRepo;

    @Autowired
    private ComicBookRepository bookRepo;

    @Autowired
    private ComicSeriesRepository seriesRepo;

    @Autowired
    private PageRepository pageRepo;

    @RequestMapping(value = "/savePage", method = RequestMethod.POST)
    public ModelAndView savePage(@RequestParam("json") String json,
                                 @RequestParam("series") String seriesID,
                                 @RequestParam("book") String bookID,
                                 @RequestParam("page") String pageNum) {
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {

        }

        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
        ComicBook book = bookRepo.findByBookID(bookID);
        Integer pageIndex = Integer.parseInt(pageNum)-1;

        String pageID = book.getPages().get(pageIndex);
        Page page = pageRepo.findByPageID(pageID);

        page.setPageData(json);
        pageRepo.save(page);

        ModelAndView mv = new ModelAndView("redirect:/editbook?series="+seriesID+"&book="+bookID);
        return mv;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam("series") String seriesID, @RequestParam("book") String bookID, @RequestParam("page") String pageNum) {
        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
//        Integer bookIndex = Integer.parseInt(bookNum)-1;
//        if(bookIndex == null || bookIndex < 0 || bookIndex >= series.getBooks().size()) {
//            // end viewing comic because book number out of bounds
//
//        }
//        String bookID = series.getBooks().get(bookIndex);

        ComicBook book = bookRepo.findByBookID(bookID);
        Integer pageIndex = Integer.parseInt(pageNum)-1;
        if(pageIndex == null || pageIndex < 0 || pageIndex >= book.getPages().size()) {
            // end vieweing comic because page number out of bounds
        }
        String pageID = book.getPages().get(pageIndex);
        System.out.println(pageNum);
        Page page = pageRepo.findByPageID(pageID);
        String pageJson = page.getPageData();

        ModelAndView mv = new ModelAndView();
        mv.setViewName("edit");
        mv.addObject("page", pageJson);
        mv.addObject("seriesID", seriesID);
        mv.addObject("bookID", bookID);
        mv.addObject("pageNum", pageNum);
        return mv;
    }

    @RequestMapping(value = "/editbook", method = RequestMethod.GET)
    public ModelAndView editBooks(@RequestParam("series") String seriesID, @RequestParam("book") String bookID){

        ModelAndView mv = new ModelAndView();
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            //user not logged in, cant
        }

        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(username);

        ComicSeries series = seriesRepo.findBySeriesID(seriesID);

        ComicBook book = bookRepo.findByBookID(bookID);

        List<Page> pages = new ArrayList<>();
        for(String pageID : (book.getPages())) {
            Page currPage = pageRepo.findByPageID(pageID);
            pages.add(currPage);
        }

        mv.setViewName("editbook");
        mv.addObject("seriesID", seriesID);
        mv.addObject("bookID", bookID);
        mv.addObject("pages",pages);
        return mv;

    }

    @RequestMapping(value = "/editseries", method = RequestMethod.GET)
    public ModelAndView editSeries(@RequestParam("series") String seriesID){
        ModelAndView mv = new ModelAndView();
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {

        }       //user not logged in, cant
        String userName = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userRepo.findByUsername(userName);
        ComicSeries currentSeries = seriesRepo.findBySeriesID(seriesID);
        if(!currentSeries.getAcctID().equals((user.getAcctID()))){
            //user not authorized
            //return to view series
            //popup message saying you are not authorized to edit series
        }
        List<ComicBook> books = new ArrayList<>();
        for(String currBookID: currentSeries.getBooks()){
            ComicBook book = bookRepo.findByBookID(currBookID);
            books.add(book);
        }
        mv.setViewName("editseries");
        mv.addObject("seriesID", seriesID);
        mv.addObject("books", books);
        mv.addObject("numfans", currentSeries.getNumFans());
        mv.addObject("numbooks", currentSeries.getNumBooks());
        mv.addObject("likes", currentSeries.getLikes());
        mv.addObject("dislikes", currentSeries.getDislikes());

        return mv;
    }





//    @RequestMapping(value = "/edit", method = RequestMethod.POST)
//    public ModelAndView editSeries(){
//        ModelAndView modelAndView = new ModelAndView();
//        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
//            //user not logged in, cant
//        }
//
//
//        modelAndView.setViewName("editSeries");
//        return modelAndView;
//
//    }

   // @RequestParamquestMapping(value = "/saveCurrPage", method)

}
