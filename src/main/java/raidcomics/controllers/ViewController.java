package raidcomics.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import raidcomics.models.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ViewController {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ComicSeriesRepository seriesRepo;
    @Autowired
    private ComicBookRepository bookRepo;
    @Autowired
    private PageRepository pageRepo;

    @RequestMapping(value = {"/view"}, method = RequestMethod.GET)
    public ModelAndView home(@RequestParam("series") String seriesID, @RequestParam("book") String bookID, @RequestParam("page") String pageNum) {
        //fetch page json from db here
        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
//        Integer bookIndex = Integer.parseInt(bookNum)-1;
//        if(bookIndex == null || bookIndex < 0 || bookIndex >= series.getBooks().size()) {
//            // end viewing comic because book number out of bounds
//
//        }
//        String bookID = series.getBooks().get(bookIndex);

        ComicBook book = bookRepo.findByBookID(bookID);
        Integer pageIndex = Integer.parseInt(pageNum)-1;
        if(pageIndex == null || pageIndex < 0 || pageIndex >= book.getPages().size()) {
            // end vieweing comic because page number out of bounds
            ModelAndView mv = new ModelAndView();
            mv.setViewName("redirect:/view?series="+seriesID+"&book="+bookID+"&page=1");
            return mv;
        }
        String pageID = book.getPages().get(pageIndex);

        Page page = pageRepo.findByPageID(pageID);
        String pageJson = page.getPageData();

        String acctID = series.getAcctID();
        User user = userRepo.findByAcctID(acctID);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("view");
        mv.addObject("page", pageJson);
        mv.addObject("seriesID", seriesID);
        mv.addObject("bookID", bookID);
        mv.addObject("pageNum", pageNum);
        mv.addObject("userName", user.getUsername());
        return mv;
    }

    /*@RequestMapping(value = {"/viewseries"}, method = RequestMethod.POST)
    public ModelAndView viewSeries(@RequestParam("series") String seriesID){
        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
        String seriesName = series.getSeriesName();
        String seriesCategory = series.getCategory();
        int likes = series.getLikes();
        int dislikes = series.getDislikes();
        int numFans = series.getNumFans();
        Date createdDate = series.getCreationDate();
        ArrayList<String> books = series.getBooks();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("viewseries");
        mv.addObject("seriesID", seriesID);
        mv.addObject("seriesName", seriesName);
        mv.addObject("seriesCategory", seriesCategory);
        mv.addObject("likes", likes);
        mv.addObject("dislikes", dislikes);
        mv.addObject("numFans", numFans);
        mv.addObject("creationDate", createdDate);
        return mv;
    }*/

    @RequestMapping(value = {"/viewbook"}, method = RequestMethod.GET)
    public ModelAndView viewBook(@RequestParam("series") String seriesID, @RequestParam("book") String bookID){
        ComicSeries series = seriesRepo.findBySeriesID(seriesID);
        ComicBook book = bookRepo.findByBookID(bookID);
        List<String> currPages = book.getPages();
        List<Page> pages = new ArrayList<>();
        for(String currPageID: book.getPages()){
            Page currPage = pageRepo.findByPageID(currPageID);
            pages.add(currPage);
        }
        String bookName = book.getBookName();
        int numLikes = book.getLikes();
        int numDislikes = book.getDislikes();
        int numPages = book.getNumPages();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("viewbook");
        mv.addObject("seriesID", seriesID);
        mv.addObject("numLikes", numLikes);
        mv.addObject("numDislikes", numDislikes);
        mv.addObject("numPages", numPages);
        return mv;
        //mv.addObject()
    }

    @RequestMapping(value = {"/viewseries"}, method = RequestMethod.GET)
    public ModelAndView viewSeries(@RequestParam("series") String seriesID){
        ModelAndView mv = new ModelAndView();
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {

        }
        ComicSeries currentSeries = seriesRepo.findBySeriesID(seriesID);
        List<ComicBook> books = new ArrayList<>();
        for(String currBookID: currentSeries.getBooks()){
            ComicBook book = bookRepo.findByBookID(currBookID);
            books.add(book);
        }

        mv.setViewName("viewseries");
        mv.addObject("seriesTitle", currentSeries.getSeriesName());
        mv.addObject("seriesID", seriesID);
        mv.addObject("series", currentSeries);
        mv.addObject("books", books);
        mv.addObject("numfans", currentSeries.getNumFans());
        mv.addObject("numbooks", currentSeries.getNumBooks());
        mv.addObject("likes",currentSeries.getLikes());
        mv.addObject("dislikes", currentSeries.getDislikes());
        return mv;

    }

    public void upVoteBook(String bookID){
        ComicBook book = bookRepo.findByBookID(bookID);
        book.setLikes(book.getLikes() + 1);
        bookRepo.save(book);
    }

    public void downVoteBook(String bookID){
        ComicBook book = bookRepo.findByBookID(bookID);
        book.setDislikes(book.getDislikes() + 1);
        bookRepo.save(book);
    }








}