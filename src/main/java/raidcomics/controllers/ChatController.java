package raidcomics.controllers;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import raidcomics.models.ChatReceive;
import raidcomics.models.ChatSent;

@RestController
public class ChatController {
    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public ModelAndView chatPage() {
        ModelAndView mv = new ModelAndView("chat");
        return mv;
    }

    @RequestMapping(value = "/serieschat", method = RequestMethod.GET)
    public ModelAndView seriesChatPage() {
        ModelAndView mv = new ModelAndView("chat");
        return mv;
    }

    @MessageMapping("/chat")
    @SendTo("/stomp/chat")
    public ChatSent globalChat(ChatReceive chat) {
        String username = chat.getUser();
        String message = chat.getMessage();
        String series = chat.getSeries();
        return new ChatSent(message, username, series);
    }

//    @MessageMapping("/{seriesID}")
//    @SendTo("/stomp/{seriesID}")
//    public ChatSent seriesChat(ChatReceive chat, @DestinationVariable String seriesID) {
//        System.out.println(seriesID);
//        String username = chat.getUser();
//        String message = chat.getMessage();
//        return new ChatSent(message, username, seriesID);
//    }
}
